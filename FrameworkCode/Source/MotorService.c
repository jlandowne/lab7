/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MotorService.h"
#include "ADService.h"
#include "EncoderService.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/
#define PeriodInMS 1        //1000Hz frequency, to be further divided later
#define PWMTicksPerMS 1250  //40 MHz clock * 32 prescaler * 1000 ms/us
#define TicksPerMS 40000    //40MHz clock * 1000 ms/us
#define BitsPerNibble 4
#define GenANormal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO) //normal generated pulses

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static int32_t  SumError = 0; //Integral of error
static float    Kp = 5;       //Proportional Gain
static float    Ki = 0.01;    //Integral Gain
static float    DesiredDC;    //Target Duty Cycle
static int32_t  RPMError = 0; //Error between actual and desired RPM

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

//Function to initialize PWM port, PB6
void PWMInit()
{
  // start by enabling the clock to the PWM Module (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {}
  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;
  // program generators to go to 1 at rising compare A, 0 on falling compare A
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenANormal;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = (((PeriodInMS * PWMTicksPerMS)) >> 1) / 3;

  // Set the initial Duty cycle on A to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;

  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN);
  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT6HI);
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0xf0ffffff) + (4 << (6 * BitsPerNibble));
  // Enable pins 6 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT6HI);
  // make pins 6 on Port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT6HI);
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | PWM_0_CTL_GENAUPD_LS);
}

//Function to set Duty Cycle of 100%
void Set100DC(void)
{
// To program 100% DC, simply set the action on Zero to set the output to one
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ONE;
// don't forget to restore the proper actions when the DC drops below 100%
// or rises above 0%
}

//Function to set Duty Cycle of 0%
void Set0DC(void)
{
// To program 0% DC, simply set the action on Zero to set the output to zero
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
// don't forget to restore the proper actions when the DC drops below 100%
// or rises above 0%
}

//Function to restore DC value when leaving 0 or 100% mode
void RestoreDC(void)
{
// To restore the previos DC, simply set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenANormal;
}

//Function to initialize Periodic Timer
void InitPeriodicInt(void)
{
// start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0; // kill a few cycles to let the clock get going
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {}
// make sure that timer (Timer B) is disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
// set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
// set up timer A in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) = (HWREG(WTIMER0_BASE + TIMER_O_TBMR) & ~TIMER_TBMR_TBMR_M) | TIMER_TBMR_TBMR_PERIOD;
// set timeout to 2 ms
  HWREG(WTIMER0_BASE + TIMER_O_TBILR) = TicksPerMS * 2;
// enable a local timeout interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
// enable the Timer B in Wide Timer 0 interrupt in the NVIC
// it is interrupt number 95 so appears in EN2 at bit 31
  HWREG(NVIC_EN2) |= BIT31HI;
  //Set priority as lower in NVIC
  HWREG(NVIC_PRI23) = (HWREG(NVIC_PRI23) & ~NVIC_PRI23_INTB_M) | (1 < NVIC_PRI23_INTB_S);
// make sure interrupts are enabled globally
  __enable_irq();
// now kick the timer off by enabling it and enabling the timer to
// stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void PWMWrite(uint32_t DC)
{
  if (DC == 0)      //If DC is desired to 0%
  {
    Set0DC();   //Call 0% DC function
  }
  else if (DC == 100)   //If DC is desired to 100%
  {
    Set100DC();   //Call 100% DC function
  }
  else
  {
    RestoreDC();    //Start by restoring DC in case it just came out of 0 or 100 mode
    //Calculate PWM val based on percentage passed and the period
    // Load - 1/2*DutyCycle*Period/2
    uint32_t PWMWriteVal = ((((PeriodInMS * PWMTicksPerMS)) >> 1) - (DC * (PeriodInMS * PWMTicksPerMS) >> 1) / 100) / 3;
    HWREG(PWM0_BASE + PWM_O_0_CMPA) = PWMWriteVal;   //Write value to compare A
  }
}

/****************************************************************************
 Function
     InitMotorService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitMotorService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event

  PWMInit(); // Call init functions
  InitPeriodicInt();

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotorService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMotorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMotorService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMotorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == PRINT_TIMER)) //If Timeout of Print timer
  {
    printf("\r\nActRPM: %d  DesRPM: %d  Err: %d  DesDC: %f SumError: %d", QueryActualRPM(), QueryDesiredRPM(), RPMError, DesiredDC, SumError);  //Print vals to TerraTerm
    ES_Timer_InitTimer(PRINT_TIMER, 500);                                                                                                       //Restart timer
  }
  /********************************************
   in here you write your service code
   *******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void PIDUpdate(void)
{
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI);
  // start by clearing the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_TBTOCINT;

  //Calculate Error in RPM
  RPMError = QueryDesiredRPM() - QueryActualRPM();
  SumError += RPMError; //Adding to error sum

  DesiredDC = Kp * (RPMError + Ki * SumError); //Calculating Desired Duty Cycle

  //Anti-Windup Measures for integration
  if (DesiredDC > 100) //If DesireDC is larger than max possible DC
  {
    DesiredDC = 100;      //Set to max
    SumError -= RPMError; //Take out last error term in sum
  }
  else if (DesiredDC < 0) //If DesireDC is smaller than min possible DC
  {
    DesiredDC = 0;        //Set to min
    SumError -= RPMError; //Take out last error term in sum
  }
  PWMWrite(((uint32_t)DesiredDC)); //Set PWM with desired DC
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT1LO);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
