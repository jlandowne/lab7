/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ADService.h"
#include "ADMulti.h"
#include "MotorService.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
#define SAMPLE_TIME 100 //ms between AD samples
#define AD_MAX 4095     //max value of AD converter

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint32_t ADValue;            //Value given by ADConverter
static uint32_t TopSpeed = 59;      //Top speed at 100%DC
static uint32_t DesiredRPM;         //PWM Value, aka Duty Cycle

/*------------------------------ Module Code ------------------------------*/

//Function to return Desired RPM value
uint32_t QueryDesiredRPM(void)
{
  return DesiredRPM;
}

/****************************************************************************
 Function
     InitADService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitADService(uint8_t Priority)
{
  ADC_MultiInit(1);                             //Init AD converter with 1 pin
  ES_Timer_InitTimer(ADTIMER, SAMPLE_TIME);     //Start Timers for AD reading and stepping

  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostADService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostADService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunADService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunADService(ES_Event_t ThisEvent)
{
  /********************************************
 in here you write your service code
 *******************************************/

  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == ADTIMER)) //If Timeout with ADTimer parameter
  {
    uint32_t ConversionResults[1];            //Create Array to modify with AD read
    ADC_MultiRead(ConversionResults);         //Read AD value on pin
    ADValue = ConversionResults[0];           //Grab AD value
    if (ADValue < 10)                         //Forcing speed to be 0 at low AD values
    {
      ADValue = 0;
    }

    DesiredRPM = ADValue * TopSpeed / AD_MAX;         //Scale AD value to an RPM value between 0 and TopSpeed

    ES_Timer_InitTimer(ADTIMER, SAMPLE_TIME); //Restart ADTimer
  }
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
