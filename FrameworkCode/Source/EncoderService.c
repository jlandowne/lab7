/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "EncoderService.h"
#include "ADService.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"

#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint32_t Period;       //Period of encoder High time, in ticks
static uint32_t LastCapture;  //Time of last captured rising edge
static uint32_t LEDNum;       //Number of LEDs (0-8) to be lit on bar
static uint32_t ActualRPM;    //Speed of moror in RPM

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

//Function to initialize Input Capture pin
void InitIC(void)
{
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to both edges, we need to modify the TAEVENT bits
  // in GPTMCTL.  First, clear the TAEVENT bits
  // Then modify to respond to Rising and Falling edges
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEVENT_BOTH;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xfff0ffff) + (7 << 16);
  // Enable pin on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI;
  // make pin 4 on Port C into an input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 and 95 so appears in EN2 at bit 30 and 31
  HWREG(NVIC_EN2) |= (BIT30HI);
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

//Function to light up LEDs according to number desired
void LightLEDs(void)
{
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI);

  //Calculate based on linear scaling of max and min Period values
  //Values are between 100 and 800 to avoid need for float data types
  LEDNum = (5 * Period - 4000) / 182;

  if (Period > 40000)  //If period is very high, motor is not moving, so set to 8 LEDs
  {
    LEDNum = 800;
  }

  HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~(BIT2HI | BIT3HI | BIT4HI); //Set all lines LO first
  HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~(BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  //Cases from 1 LED to 8 LEDs
  if (LEDNum >= 700)
  {
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI | BIT3HI | BIT4HI);
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  }
  else if (LEDNum >= 600)
  {
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT3HI | BIT4HI);
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  }
  else if (LEDNum >= 500)
  {
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI);
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  }
  else if (LEDNum >= 400)
  {
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  }
  else if (LEDNum >= 300)
  {
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI);
  }
  else if (LEDNum >= 200)
  {
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI);
  }
  else if (LEDNum >= 180)
  {
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI);
  }
  else
  {
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI);
  }
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~(BIT1HI);
}

//Function to Calculate Actual RPM values using encoder input
void CalculateRPM(void)
{
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI);

  ActualRPM = 1250 * 1000 * 30 / Period;  //Calculation of RPM based on period (see calcs. page)
  ActualRPM = ActualRPM * 59 / 5120;      //Complete calc in two steps to avoid overflow

  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~(BIT1HI);
}

//Function to return Actual RPM
uint32_t QueryActualRPM(void)
{
  return ActualRPM;
}

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitEncoderService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitEncoderService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  //Set up LED Pins
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5;                         //enable Port F
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5) //wait for Port F to initialize
  {}
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI);                    //assign Port F Pins bit 1-4 as digital I/O, output
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI);
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);  //assign Port E Pins bit 1-5 as digital I/O, output
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= (BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  InitIC();                             //Init Input Capture
  ES_Timer_InitTimer(PRINT_TIMER, 500); //Start timer to print RPM values ever .5 sec

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostEncoderService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEncoderService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunEncoderService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunEncoderService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
  if (ThisEvent.EventType == ES_NEW_SPEED)  //If New Speed Event
  {
    LightLEDs();    //Update LED lighting
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

//ISR for input capture
void InputCaptureResponse(void)
{
  uint32_t ThisCapture;  //Create ThisCapture to track current time of interrupt

// start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  //Grab captured value of interrupt time
  ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);

  if ((HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI) != 0)  // If Pin is HI (start of High Time Period)
  {
    LastCapture = ThisCapture; //Update Last Capture
  }
  else   //If Pin is LO (end of High Time Period)
  {
    Period = ThisCapture - LastCapture; //Update Period as Time of Fall - Time of Rise
    ES_Event_t ThisEvent;               //Post Event to Encoder Service
    ThisEvent.EventType = ES_NEW_SPEED;
    PostEncoderService(ThisEvent);
    CalculateRPM(); //Update RPM value
  }
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
